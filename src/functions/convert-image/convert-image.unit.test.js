const convertImage = require(".");
const validPngEvent = require("./mocks/valid-png-image.json");
const validJpgEvent = require("./mocks/valid-jpg-image.json");
const nonExistantImageEvent = require("./mocks/non-existant-image.json");
const invalidFileTypeEvent = require("./mocks/invalid-file-type.json");

describe("when valid png data is passed", () => {
  it("should return a 200 status code", async () => {
    const response = await convertImage.handler(validPngEvent);

    expect(response.statusCode).toBe(200);
  });
});

describe("when valid jpg data is passed", () => {
  it("should return a 200 status code", async () => {
    const response = await convertImage.handler(validJpgEvent);

    expect(response.statusCode).toBe(200);
  });
});

describe("when we can't find the file", () => {
  it("should return a 500 status code", async () => {
    const response = await convertImage.handler(nonExistantImageEvent);

    expect(response.statusCode).toBe(500);
    expect(response.body).toBe("Image not found!");
  });
});

describe("when an invalid file type is passed", () => {
  it("should return a 500 status code", async () => {
    const response = await convertImage.handler(invalidFileTypeEvent);

    expect(response.statusCode).toBe(500);
    expect(response.body).toBe("Unsupported file type!");
  });
});

describe("when null is passed", () => {
  it("should return a 500 status code", async () => {
    const response = await convertImage.handler(null);

    expect(response.statusCode).toBe(500);
    expect(response.body).toBe("Cannot read property 'Records' of null");
  });
});
