const jimp = require("jimp");
const S3Client = require("aws-sdk/clients/s3");
const util = require("util");

const s3 = new S3Client();

async function fetchObjectFromBucket(bucket, key) {
  try {
    console.log(`Trying to retrieve\nkey: ${key}\nfrom bucket: ${bucket}`);
    const s3Object = await s3
      .getObject({
        Bucket: bucket,
        Key: key
      })
      .promise();

    //console.log(`s3 object: ${util.inspect(s3Object, { depth: 5 })}`);
    return s3Object;
  } catch (error) {
    throw new Error("Image not found!");
  }
}

function convertJpgToPng(image) {
  console.log("Handling jpg...");
  return new Promise((resolve, reject) => {
    image.getBuffer(jimp.MIME_PNG, (error, img) => {
      if (error) {
        reject(error);
      } else {
        resolve(img);
      }
    });
  });
}

function convertPngtoJpg(image) {
  console.log("Handling png...");
  return new Promise((resolve, reject) => {
    image.getBuffer(jimp.MIME_JPEG, (error, img) => {
      if (error) {
        reject(error);
      } else {
        resolve(img);
      }
    });
  });
}

async function convertImage(bucket, key) {
  // Infer the image type.
  const typeMatch = key.match(/([a-zA-Z0-9\s_\\.\-\(\):]+)\.([^.]*)$/);
  if (!typeMatch) {
    throw new Error(`Could not understand file name! - ${key}`);
  }
  const fileName = typeMatch[1];
  const fileType = typeMatch[2];
  console.log(`File Name: ${fileName}`);
  console.log(`File Type: ${fileType}`);

  if (fileType !== "jpg" && fileType !== "jpeg" && fileType !== "png") {
    throw new Error("Unsupported file type!");
  }

  const s3Object = await fetchObjectFromBucket(bucket, key);

  const image = await jimp.read(s3Object.Body);

  //const convertedImage = await
  let newImage;
  switch (fileType) {
    case "jpg":
    case "jpeg":
      newImage = await convertJpgToPng(image);
      break;
    case "png":
      newImage = await convertPngtoJpg(image);
      break;
    default:
      throw new Error("Unsupported file type");
  }

  console.log(
    `New Image: ${util.inspect(newImage, { depth: 5, colors: false })}`
  );

  var params = {
    Body: newImage,
    Bucket: bucket,
    Key: `${fileName}.${fileType === "png" ? "jpg" : "png"}`
  };

  const putResponse = await s3.putObject(params).promise();

  return putResponse;
}

module.exports.handler = async (event) => {
  try {
    const record = event.Records[0];
    // Read options from the event.

    const srcBucket = record.s3.bucket.name;
    console.log(`Source Bucket: ${srcBucket}`);

    // Object key may have spaces or unicode non-ASCII characters.
    const srcKey = decodeURIComponent(record.s3.object.key.replace(/\+/g, " "));
    console.log(`Source Key: ${srcKey}`);

    const response = await convertImage(srcBucket, srcKey);

    return {
      statusCode: 200,
      body: JSON.stringify(
        {
          response,
          input: event
        },
        null,
        2
      )
    };
  } catch (error) {
    console.log(`caught error: ${util.inspect(error, { depth: 5 })}`);
    return {
      statusCode: 500,
      body: error.message
    };
  }
};
