module.exports = {
  coverageThreshold: {
    global: {
      branches: 50,
      functions: 50,
      lines: 50,
      statements: 50
    }
  },
  collectCoverage: false,
  collectCoverageFrom: ["./src/**/*.js", "!**/node_modules/**"],
  reporters: ["default"],
  testTimeout: 10000,
  verbose: true
};
